document.addEventListener('DOMContentLoaded', function() {
  let slider = document.querySelector('.slider');
  let forgot = document.querySelector('.forgot');
  let nextButton = document.querySelector('.next');
  let loginButton = document.querySelector('.login')
  let emailInput = document.querySelector('[type="email"]');
  let passwordInput = document.querySelector('[type="password"]');
  let username = document.querySelector('.username');
  let back = document.querySelector('.user');

  emailInput.focus();
  
  nextButton.addEventListener('click', function() {
    gotoPassword();
  });

  back.addEventListener('click', function() {
    gotoEmail();
  });

  emailInput.addEventListener('input', function(event) {
    username.innerHTML = event.target.value;
    nextButton.disabled = ! event.target.value === '';
  });

  emailInput.addEventListener('keydown', function(event) {
    if (event.keyCode === 13 && !nextButton.disabled) {
      gotoPassword();
    }
  });

  passwordInput.addEventListener('input', function(event) {
    loginButton.disabled = !event.target.value === '';
  });

  passwordInput.addEventListener('keydown', function(event) {
    if (event.keyCode === 13 && !loginButton.disabled) {
      gotoUrl();
    }
  });

  loginButton.addEventListener('click', function() {
    gotoUrl();
  });

  function gotoPassword() {
    slider.style.transform = 'translateX(-50%)';
    forgot.style.display = 'none';

    setTimeout(function() {
      passwordInput.focus();
    }, 500);
  }

  function gotoEmail() {
    slider.removeAttribute('style');
    forgot.removeAttribute('style');

    setTimeout(function() {
      emailInput.focus();
    }, 500);
  }

  function gotoUrl() {
    window.location = loginButton.getAttribute('data-url');
  }
});
